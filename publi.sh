#!/bin/bash

exportdir=$1
stylesdir="asciidoctor-stylesheet-factory"
stylename="OffRoad"

[ ! -d "${exportdir}" ] && mkdir ${exportdir}
# clone asciidoctor stylesheet (we are dependent on some these)
[ ! -d "${stylesdir}" ] && git clone https://github.com/asciidoctor/asciidoctor-stylesheet-factory.git ${stylesdir}
# add our stylesheet to those
cp -R sass ${stylesdir}/
# compile our stylesheet (and every other stylesheet)
echo "Generating stylesheets ..."
cd ${stylesdir} && compass compile && cd -
# copy compiled css to export directory
cp "${stylesdir}/stylesheets/${stylename}.css" "${exportdir}/${stylename}.css"
# generate HTML
echo "Generating HTML ..."
asciidoctor     chargen.adoc -a linkcss -a stylesheet="${stylename}.css" -o ${exportdir}/index.html
asciidoctor     craft.adoc -a linkcss -a stylesheet="${stylename}.css" -o ${exportdir}/craft.html
# cleanup what was cloned
rm -rf ${stylesdir}
