= CŒUR: Les personnages
:toc: left
:toc-title: Table des matières
:toclevels: 2

> **C**hroniques **O**ubliées : **É**tendu, **U**nifié et **R**afistolé



== Avant propos

Ce document à pour objectif de rendre le système de jeu des link:https://www.black-book-editions.fr/catalogue.php?id=13[Chroniques Oubliés] plus *simple*, *flexible*, et *inclusif*.
Cela passe par la *disparition des profils et niveaux*, un *<<chargen,création de personnage simplifiée>>*, et un *<<xp,système d'expérience>>* permettant entre autres de découpler la progression d'un personnage et l'avancement de ses facultés martiales.

Pour en savoir davantage sur mes objectifs, rendez-vous <<design,en fin de document>>.

D'ici là, bonne lecture !

<<<



:leveloffset: +1

include::chapters/chargen/chargen.adoc[]

include::chapters/chargen/xp.adoc[]

include::chapters/chargen/traits.adoc[]

include::chapters/chargen/paths.adoc[]

include::chapters/chargen/gear.adoc[]

[[design]]
= Notes de conception
include::chapters/chargen/design.adoc[]

include::chapters/legal.adoc[]

:leveloffset: -1
