[[xp]]
= Expérience

[[retcon]]
.Rejouer le match
**** 
Parfois, on peut regretter ses choix de progression.
Avec l'accord du MJ, les PX mal dépensés peuvent être récupérés.
Cela peut demander une intrigue dédiée : recherche d'un entraîneur, quête personnelle, ...
****

À divers moments que le ou la MJ juge opportuns, il ou elle distribue des Points d'expérience (PX) à ses joueurs.
Ce peut être par exemple à la fin d'une séance de jeu, ou à la fin d'une scène mémorable de l'aventure.
Ces PX peuvent récompenser l'interprétation, la prise de risque, la complétion des objectifs d'un scénario, et ainsi de suite.

Chaque joueuse ou joueur peut dépenser tout ou partie de ses PX pour améliorer son personnage.
Il est évidemment impossible de dépenser davantage de PX que ceux qu'on a gagné.
Si une joueuse ou un joueur n'a pas assez de PX pour améliorer son personnage, cela ne signifie pas forcément que celui-ci ne s'entraîne pas ; simplement que l'expérience met du temps à porter ses fruits.

Le coût en PX de chaque amélioration est décrit dans la table suivante.

.Coûts en Points d'expérience
[options="header", width=50%, cols="65,^.15"]
|===
|Amélioration                        |Coût

| ATC +1                             | 2
| ATD +1                             | 2
| ATM +1                             | 2
| PV +3                              | 1
| Un <<traits,trait>> au choix       | 1
| Capacité de <<voies,voie>> de rang 1 ou 2         | 1
| Capacité de <<voies,voie>> de rang 3, 4 ou 5      | 2
| +1 à une des six caractéristiques  | 4
|===

[[caps]]
.Dépasser les bornes des limites
****
À la suite d'un événement marquant pour tous les PJs du genre révélation importante ou combat épique, la limite sur le nombre d'améliorations peut passer à 10, voire disparaître.
Des capacites de voie link:craft.html#voies[de rang 6 ou davantage] peuvent alors apparaître.
****

À la création de personnage, il est impossible d'augmenter une des six caractéristiques ou d'acheter une capacité de <<voies,voie>> de rang supérieur à 2.



== Limites

Chaque MJ peut limiter le nombre maximum d'améliorations pouvant être achetées.
Par exemple, en début de campagne, chaque aspect de la fiche de personnage peut être limité à 5 au maximum : attaques, caractéristiques, rangs de <<voies,voies>>, +30 PV max, ...

Ces limites peuvent par exemple permettre d'inciter les personnages expérimentés à se diversifier, ou à niveler les puissances des personnages expérimentés et débutants. +
Il s'agit plus ou moins à l'identique de la notion de link:https://olddungeonmaster.com/2014/08/30/bounded-accuracy/[_bounded accuracy_].
